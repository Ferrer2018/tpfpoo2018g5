package poo2018.fi.unju.edu.ar.trabajopracticofinal.domain;

import java.io.Serializable;

public class Producto implements Serializable {
    private int idproducto;
    private String nombre;
    private  int tamaño;
    private double precioU;
    private String estado;
    private int stock;

    public Producto(int idproducto, String nombre, int tamaño, double precioU, String estado, int stock) {
        this.idproducto = idproducto;
        this.nombre = nombre;
        this.tamaño = tamaño;
        this.precioU = precioU;
        this.estado = estado;
        this.stock = stock;
    }

    public Producto() {
    }

    public int getIdproducto() {
        return idproducto;
    }

    public void setIdproducto(int idproducto) {
        this.idproducto = idproducto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getTamaño() {
        return tamaño;
    }

    public void setTamaño(int tamaño) {
        this.tamaño = tamaño;
    }

    public double getPrecioU() {
        return precioU;
    }

    public void setPrecioU(double preciounitario) {
        this.precioU = precioU;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }
}
