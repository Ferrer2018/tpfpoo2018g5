package poo2018.fi.unju.edu.ar.trabajopracticofinal.domain;

public class Cliente {
    private int idcliente;
    private String cuit;
    private String nombre;
    private String domicilio;

    public Cliente(int idcliente, String cuit, String nombre, String domicilio) {
        this.idcliente = idcliente;
        this.cuit = cuit;
        this.nombre = nombre;
        this.domicilio = domicilio;
    }

    public Cliente() {
    }

    public int getIdcliente() {
        return idcliente;
    }

    public void setIdcliente(int idcliente) {
        this.idcliente = idcliente;
    }

    public String getCuit() {
        return cuit;
    }

    public void setCuit(String cuit) {
        this.cuit = cuit;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }
}

