package poo2018.fi.unju.edu.ar.trabajopracticofinal.BDSqLite.ConsultaSQLite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import poo2018.fi.unju.edu.ar.trabajopracticofinal.BDSqLite.SQLiteHelper;
import poo2018.fi.unju.edu.ar.trabajopracticofinal.domain.Pedido;
import poo2018.fi.unju.edu.ar.trabajopracticofinal.BDSqLite.util.TablaBD;

public class SqLiteAdminPedido {

    private SQLiteHelper sqLiteHelper;
    private TablaBD tablaBD;
    private SQLiteDatabase db;

    /**
     * Constructor.
     *
     * @param context
     */

    public SqLiteAdminPedido(Context context) {
        //nos crea la base de datos
        sqLiteHelper = new SQLiteHelper(context);
    }

    /**
     * Abre la conexión con la base de datos.
     */

    public void abrir() {
        db = sqLiteHelper.getWritableDatabase();
    }

    /**
     * Consulta la conexión de la base de datos.
     */

    public void lectura() {
        sqLiteHelper.getReadableDatabase();
    }

    /**
     * Cierra la conexión de la base de datos.
     */

    public void cerrar() {
        db.close();
    }

    /**
     * Inserta un registro en la tabla Pedidos
     * @param pedido
     * @return
     */
    public long insertarPedido(Pedido pedido) {
        ContentValues valores = new ContentValues();
        valores.put(tablaBD.PEDIDO_FECHA, pedido.getFechapedido());
        valores.put(tablaBD.PEDIDO_ESTADO, pedido.getEstadopedido());
        return db.insert(tablaBD.TABLA_PEDIDO, null, valores);
    }

    /**
     * Modifica un registro de la tabla Productos.
     * @param pedido
     * @return
     */
    public long modificarPedido(Pedido pedido){
        ContentValues contentValues = new ContentValues();
        contentValues.put(tablaBD.PEDIDO_ID,pedido.getIdpedido());
        contentValues.put(tablaBD.PEDIDO_ID_VENDEDOR,pedido.getUsuario().getId());
        contentValues.put(tablaBD.PEDIDO_FECHA,pedido.getFechapedido());
        contentValues.put(tablaBD.PEDIDO_ESTADO,pedido.getEstadopedido());
        contentValues.put(tablaBD.PEDIDO_PRODUCTO_ID,pedido.getProducto().getIdproducto());
        contentValues.put(tablaBD.PEDIDO_CLIENTE_ID,pedido.getCliente().getIdcliente());
        return db.update(tablaBD.TABLA_PEDIDO, contentValues, tablaBD.PRODUCTO_ID +"=" + pedido.getIdpedido(), null);
    }

    /*public ArrayList<Pedido> getPedidos() {
        ArrayList<Pedido> lista = new ArrayList<>();
        lectura();
        String cadenaQuery = "SELECT * FROM " + tablaBD.TABLA_PEDIDO;
        Cursor cursor = db.rawQuery(cadenaQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                int idpedido = cursor.getInt(cursor.getColumnIndex("idpedido"));
                String estadopedido = cursor.getString(cursor.getColumnIndex("estadopedido"));
                String fechapedido = cursor.getString(cursor.getColumnIndex("fechapedido"));
                String producto = cursor.getColumnName(cursor.getColumnIndex("producto"));
                String usuario = cursor.getColumnName(cursor.getColumnIndex("usuario"));
                String cliente = cursor.getColumnName(cursor.getColumnIndex("cliente"));
                double total = cursor.getDouble(cursor.getColumnIndex("total"));
                Pedido pedido = new Pedido(idpedido,estadopedido,fechapedido,producto,usuario,cliente,total);
                lista.add(pedido);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return lista;
    }*/
}
