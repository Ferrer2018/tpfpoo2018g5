package poo2018.fi.unju.edu.ar.trabajopracticofinal.util;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import poo2018.fi.unju.edu.ar.trabajopracticofinal.R;
import poo2018.fi.unju.edu.ar.trabajopracticofinal.domain.Cliente;

public class AdaptadorTablaCliente extends BaseAdapter {

    ArrayList<Cliente> itemList;
    Context context;

    public AdaptadorTablaCliente(Context c, ArrayList<Cliente> arrayCliente){
        context = c;
        itemList = arrayCliente;
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public Object getItem(int position) {
        return itemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.item_tabla_cliente, parent, false);
        TextView lblCuitC =(TextView) itemView.findViewById(R.id.lblCuitC);
        TextView lblNombreC =(TextView) itemView.findViewById(R.id.lblNombreC);
        TextView lblDomicilioC =(TextView) itemView.findViewById(R.id.lblDomicilioC);
        lblCuitC.setText(""+itemList.get(position).getCuit());
        lblNombreC.setText(""+itemList.get(position).getNombre());
        lblDomicilioC.setText(""+itemList.get(position).getDomicilio());
        if (position%2==0){
            itemView.setBackgroundColor(Color.argb(255,248,248,248));
        } else {
            itemView.setBackgroundColor(Color.argb(255,255,255,255));
        }
        return itemView;
    }
}
