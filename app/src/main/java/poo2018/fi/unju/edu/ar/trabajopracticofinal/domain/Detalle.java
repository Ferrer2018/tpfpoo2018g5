package poo2018.fi.unju.edu.ar.trabajopracticofinal.domain;

public class Detalle {

    private int id;
    private Producto producto;
    private int cantidad;
    private int numeroitem;

    public Detalle(int id, Producto producto, int cantidad, int numeroitem) {
        this.id = id;
        this.producto = producto;
        this.cantidad = cantidad;
        this.numeroitem = numeroitem;
    }

    public Detalle() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getNumeroitem() {
        return numeroitem;
    }

    public void setNumeroitem(int numeroitem) {
        this.numeroitem = numeroitem;
    }

    public double subTotal(){
        return cantidad*producto.getPrecioU();
    }
}