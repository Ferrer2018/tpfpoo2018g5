package poo2018.fi.unju.edu.ar.trabajopracticofinal.BDSqLite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import poo2018.fi.unju.edu.ar.trabajopracticofinal.BDSqLite.util.TablaBD;

/**
 * Clase que crea la base de datos y permite la conexión con la BD.
 *
 * @author Maxi-Diaz
 */

public class SQLiteHelper extends SQLiteOpenHelper{

    private static final String NAME_DATABASE = "dbPreventas";
    private static final int VERSION = 1;

    /**
     * Constructor
     * @param context
     */

    public SQLiteHelper(Context context){
        super(context,NAME_DATABASE,null,VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TablaBD.CREAR_TABLA_USUARIO);
        db.execSQL(TablaBD.CREAR_TABLA_PRODUCTO);
        db.execSQL(TablaBD.CREAR_TABLA_CLIENTE);
        db.execSQL(TablaBD.CREAR_TABLA_PEDIDO);
        db.execSQL(TablaBD.CREAR_TABLA_DETALLE);
        db.execSQL(TablaBD.defaultUser);
        db.execSQL(TablaBD.defaultUser1);
        db.execSQL(TablaBD.defaultProducto);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (newVersion > oldVersion){
            db.execSQL("DROP TABLE IF EXIST " + TablaBD.CREAR_TABLA_USUARIO);
            db.execSQL("DROP TABLE IF EXIST " + TablaBD.CREAR_TABLA_PRODUCTO);
            db.execSQL("DROP TABLE IF EXIST " + TablaBD.CREAR_TABLA_CLIENTE);
            db.execSQL("DROP TABLE IF EXIST " + TablaBD.CREAR_TABLA_PEDIDO);
            db.execSQL("DROP TABLE IF EXIST " + TablaBD.CREAR_TABLA_DETALLE);
            onCreate(db);
        }
    }
}
