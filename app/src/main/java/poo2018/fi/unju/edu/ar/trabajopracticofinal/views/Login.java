package poo2018.fi.unju.edu.ar.trabajopracticofinal.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import poo2018.fi.unju.edu.ar.trabajopracticofinal.Interfaces.ILogin;
import poo2018.fi.unju.edu.ar.trabajopracticofinal.Presenters.LoginPresenterImpl;
import poo2018.fi.unju.edu.ar.trabajopracticofinal.R;

public class Login extends AppCompatActivity implements ILogin.view{

    private TextView txtRegistrar;
    private EditText user, pass;
    private ILogin.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //captura de datos
        txtRegistrar = findViewById(R.id.txtRegistrar);
        txtRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), RegistrarActivity.class);
                startActivity(intent);
            }
        });
        pass = findViewById(R.id.editText3);
        presenter = new LoginPresenterImpl(this,getApplicationContext());
    }

    @Override
    public void setErrorPassword() {
        pass.setError("Campo Obligatorio");
    }

    @Override
    public void showPassword() {
        Toast.makeText(this, "Usuario Correcto", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showPasswordError() {
        Toast.makeText(this, "Usuario Incorrecto", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void navigateToHomeAdmin() {
        startActivity(new Intent(Login.this, MenuAdministrador.class));
    }

    @Override
    public void navigateToHomeVendedor() {
        startActivity(new Intent(Login.this,MenuVendedor.class));
    }

    public void validacion(View view){
        presenter.validarUsuario(pass.getText().toString());
    }
}
