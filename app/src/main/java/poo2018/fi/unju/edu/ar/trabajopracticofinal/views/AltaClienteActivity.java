package poo2018.fi.unju.edu.ar.trabajopracticofinal.views;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import poo2018.fi.unju.edu.ar.trabajopracticofinal.BDSqLite.ConsultaSQLite.SqLiteAdminCliente;
import poo2018.fi.unju.edu.ar.trabajopracticofinal.R;
import poo2018.fi.unju.edu.ar.trabajopracticofinal.domain.Cliente;

public class AltaClienteActivity extends AppCompatActivity {

    Button btnNuevo;
    EditText txtNombre, txtCuit, txtDomicilio;
    Cliente cliente;
    SqLiteAdminCliente adminCliente;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alta_cliente);
        setupActionBar();
        context = this;
        btnNuevo = findViewById(R.id.btnNuevo);
        txtNombre = findViewById(R.id.txtNombre);
        txtCuit = findViewById(R.id.txtCuit);
        txtDomicilio = findViewById(R.id.txtDomicilio);
        crearCliente();
    }

    public void crearCliente(){
        adminCliente = new SqLiteAdminCliente(this);
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adminCliente.abrir();
                cliente = new Cliente();
                cliente.setCuit(txtCuit.getText().toString());
                cliente.setNombre(txtNombre.getText().toString());
                cliente.setDomicilio(txtDomicilio.getText().toString());
                long registro = adminCliente.insertarCliente(cliente);
                if (registro > 0) {
                    Toast.makeText(context, "Registrado con exito!", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(context, "No se pudo registrar", Toast.LENGTH_LONG).show();
                }
                adminCliente.cerrar();
                finish();
            }
        });
    }

    public void cancelarCliente(View v){
        Intent activity = new Intent(this, gestionClienteActivity.class);
        startActivity(activity);
    }

    private void setupActionBar(){
        ActionBar actionBar=getSupportActionBar();
        if(actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }
}