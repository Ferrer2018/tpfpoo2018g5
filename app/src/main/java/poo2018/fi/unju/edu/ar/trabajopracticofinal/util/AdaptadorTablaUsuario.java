package poo2018.fi.unju.edu.ar.trabajopracticofinal.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import poo2018.fi.unju.edu.ar.trabajopracticofinal.R;
import poo2018.fi.unju.edu.ar.trabajopracticofinal.domain.Usuario;

public class AdaptadorTablaUsuario extends BaseAdapter {

    ArrayList<Usuario> itemList;
    Context context;

    public AdaptadorTablaUsuario(Context c, ArrayList<Usuario> arrayUsuario){
        context = c;
        itemList = arrayUsuario;
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public Object getItem(int position) {
        return itemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.item_tabla_listausuario, parent, false);
        TextView lblDni =(TextView) itemView.findViewById(R.id.lblDni);
        TextView lblNombre =(TextView) itemView.findViewById(R.id.lblNombre);
        TextView lblRol =(TextView) itemView.findViewById(R.id.lblRol);
        lblDni.setText(""+itemList.get(position).getDni());
        lblNombre.setText(""+itemList.get(position).getNombre());
        lblRol.setText(""+itemList.get(position).getRol());
        if (position%2==0){
            itemView.setBackgroundColor(Color.argb(255,248,248,248));
        } else {
            itemView.setBackgroundColor(Color.argb(255,255,255,255));
        }
        return itemView;
    }
}

