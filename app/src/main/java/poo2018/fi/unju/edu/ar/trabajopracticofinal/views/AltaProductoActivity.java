package poo2018.fi.unju.edu.ar.trabajopracticofinal.views;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import poo2018.fi.unju.edu.ar.trabajopracticofinal.BDSqLite.ConsultaSQLite.SqLiteAdminProducto;
import poo2018.fi.unju.edu.ar.trabajopracticofinal.R;
import poo2018.fi.unju.edu.ar.trabajopracticofinal.domain.Producto;

public class AltaProductoActivity extends AppCompatActivity {

    Button btnNuevo;
    EditText txtNombrep, txtStockp, txtPreciop, txtTamañop;
    RadioGroup radioGroup;
    RadioButton rdDisponiblep, rdNoDisponible;
    Producto producto;
    SqLiteAdminProducto adminProducto;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.activity_alta_producto);
        btnNuevo = findViewById(R.id.btnNuevo);
        txtNombrep = findViewById(R.id.txtNombrep);
        txtStockp = findViewById(R.id.txtStockp);
        txtPreciop = findViewById(R.id.txtPreciop);
        txtTamañop = findViewById(R.id.txtTamañop);
        rdDisponiblep = findViewById(R.id.rdDisponiblep);
        crearProducto();
    }

    public void crearProducto(){
        adminProducto = new SqLiteAdminProducto(this);
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adminProducto.abrir();
                producto = new Producto();
                producto.setNombre(txtNombrep.getText().toString());
                producto.setTamaño(Integer.parseInt(txtTamañop.getText().toString()));
                producto.setPrecioU(Double.parseDouble(txtPreciop.getText().toString()));
                producto.setEstado(rdDisponiblep.getText().toString());
                producto.setStock(Integer.parseInt(txtStockp.getText().toString()));
                long registro = adminProducto.insertarProducto(producto);
                if (registro > 0) {
                    Toast.makeText(context, "Producto fue registrado con exito!", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(context, "No se pudo registrar el Producto", Toast.LENGTH_LONG).show();
                }
                adminProducto.cerrar();
                finish();
            }
        });
    }

    public void cancelarProducto(View v){
        Intent activity = new Intent(this, MenuAdministrador.class);
        startActivity(activity);
    }

}