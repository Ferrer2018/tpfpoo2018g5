package poo2018.fi.unju.edu.ar.trabajopracticofinal.util;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import poo2018.fi.unju.edu.ar.trabajopracticofinal.R;
import poo2018.fi.unju.edu.ar.trabajopracticofinal.domain.Pedido;

public class AdaptadorTablaPedido extends BaseAdapter {

    ArrayList<Pedido> itemList;
    Context context;

    public AdaptadorTablaPedido(ArrayList<Pedido> itemList, Context context) {
        this.itemList = itemList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public Object getItem(int position) {
        return itemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.item_tabla_producto, parent, false);
        TextView lblIdPD =(TextView) itemView.findViewById(R.id.lblIdPD);
        TextView lblFechaPD =(TextView) itemView.findViewById(R.id.lblFechaPD);
        TextView lblClientePD =(TextView) itemView.findViewById(R.id.lblClientePD);
        TextView lblEstadoPD =(TextView) itemView.findViewById(R.id.lblEstadoPD);
        TextView lblTotalPD =(TextView) itemView.findViewById(R.id.lblTotalPD);
        lblIdPD.setText(""+itemList.get(position).getIdpedido());
        lblFechaPD.setText(""+itemList.get(position).getFechapedido());
        lblClientePD.setText(""+itemList.get(position).getCliente());
        lblEstadoPD.setText(""+itemList.get(position).getEstadopedido());
        lblTotalPD.setText(""+itemList.get(position).getTotal());
        if (position%2==0){
            itemView.setBackgroundColor(Color.argb(255,248,248,248));
        } else {
            itemView.setBackgroundColor(Color.argb(255,255,255,255));
        }
        return itemView;
    }
}
