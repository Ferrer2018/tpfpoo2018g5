package poo2018.fi.unju.edu.ar.trabajopracticofinal.views;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;

import poo2018.fi.unju.edu.ar.trabajopracticofinal.R;

public class SplashScreen extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashScreen.this, Login.class);
                startActivity(intent);
            }
        },3000);
    }
}
