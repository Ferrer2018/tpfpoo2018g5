package poo2018.fi.unju.edu.ar.trabajopracticofinal.Interfaces;

public interface ILogin {

    interface Interactor{
        void validarUser(String pass, Listener listener);
    }

    interface Listener{
        void passwordError();
        void showUserNoValido();

        void exitoOperacionAdmin();
        void exitoOperacionVendedor();
    }

    interface Presenter{
        void validarUsuario(String pass);
    }

    interface view{

        void setErrorPassword();
        void showPasswordError();
        void showPassword();

        void navigateToHomeAdmin();
        void navigateToHomeVendedor();
    }
}
