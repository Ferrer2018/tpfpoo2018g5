package poo2018.fi.unju.edu.ar.trabajopracticofinal.BDSqLite.ConsultaSQLite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import poo2018.fi.unju.edu.ar.trabajopracticofinal.BDSqLite.SQLiteHelper;
import poo2018.fi.unju.edu.ar.trabajopracticofinal.domain.Usuario;
import poo2018.fi.unju.edu.ar.trabajopracticofinal.BDSqLite.util.TablaBD;

/**
 * Clase que gestiona la BD de la aplicacion.
 *
 * @author Maxi-Diaz
 */

public class SqLiteAdminUsuario {

    private SQLiteHelper sqLiteHelper;
    private TablaBD tablaBD;
    private SQLiteDatabase db;

    /**
     * Constructor.
     *
     * @param context
     */

    public SqLiteAdminUsuario(Context context) {
        //nos crea la base de datos
        sqLiteHelper = new SQLiteHelper(context);
    }

    /**
     * Abre la conexión con la base de datos.
     */

    public void abrir() {
        db = sqLiteHelper.getWritableDatabase();
    }

    /**
     * Cierra la conexión de la base de datos.
     */

    public void cerrar() {
        db.close();
    }

    /**
     * Consulta la conexión de la base de datos.
     */

    public void lectura() {
       db = sqLiteHelper.getReadableDatabase();
    }

    /**
     * inserta un registro en la tabla usuario
     *
     * @param usuario
     * @return
     */

    public long insertarUsuario(Usuario usuario) {
        ContentValues valores = new ContentValues();
        valores.put(tablaBD.CAMPO_NOMBRE, usuario.getNombre());
        valores.put(tablaBD.CAMPO_PASSWORD, usuario.getPassword());
        valores.put(tablaBD.CAMPO_DNI, usuario.getDni());
        valores.put(tablaBD.CAMPO_ROL, usuario.getRol());
        return db.insert(tablaBD.TABLA_USUARIO, null, valores);
    }

    public Usuario getPassword(String pass) {
        Usuario usuario = null;
        String rol = "Administrador";
        abrir();
        String cadenaQuery = "SELECT " + tablaBD.CAMPO_PASSWORD +", "+ tablaBD.CAMPO_ROL + " FROM " + tablaBD.TABLA_USUARIO + " WHERE " + tablaBD.CAMPO_PASSWORD +
                "='" + pass + "'";
        Cursor fila = db.rawQuery(cadenaQuery, null);
        if (fila.moveToFirst()) {
            usuario = new Usuario();
            usuario.setPassword(fila.getString(0));
            usuario.setRol(fila.getString(1));
        }
        return usuario;
    }

    public ArrayList<Usuario> getUsuarios() {
        ArrayList<Usuario> lista = new ArrayList<>();
        lectura();
        String cadenaQuery = "SELECT * FROM " + tablaBD.TABLA_USUARIO;
        Cursor cursor = db.rawQuery(cadenaQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                int id = cursor.getInt(cursor.getColumnIndex("id"));
                String nombre = cursor.getString(cursor.getColumnIndex("nombre"));
                String password = cursor.getString(cursor.getColumnIndex("password"));
                String dni = cursor.getString(cursor.getColumnIndex("dni"));
                String rol = cursor.getString(cursor.getColumnIndex("rol"));
                Usuario user = new Usuario(id, nombre, password, dni, rol);
                lista.add(user);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return lista;
    }
}


