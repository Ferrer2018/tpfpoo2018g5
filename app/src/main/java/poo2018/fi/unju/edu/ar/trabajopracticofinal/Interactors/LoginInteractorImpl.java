package poo2018.fi.unju.edu.ar.trabajopracticofinal.Interactors;

import android.content.Context;
import android.os.Handler;

import poo2018.fi.unju.edu.ar.trabajopracticofinal.BDSqLite.ConsultaSQLite.SqLiteAdminUsuario;
import poo2018.fi.unju.edu.ar.trabajopracticofinal.Interfaces.ILogin;
import poo2018.fi.unju.edu.ar.trabajopracticofinal.domain.Usuario;

public class LoginInteractorImpl implements ILogin.Interactor{

    private final ILogin.Presenter presenter;
    private SqLiteAdminUsuario adminUsuario;

    public LoginInteractorImpl(ILogin.Presenter presenter, Context context){
        this.presenter = presenter;
        adminUsuario = new SqLiteAdminUsuario(context);
    }

    @Override
    public void validarUser(final String pass,final ILogin.Listener listener) {
        adminUsuario.abrir();
        Usuario usuario = adminUsuario.getPassword(pass);
        if (!pass.equals("")) {
            if (usuario == null){
                listener.showUserNoValido();
            } else {

                if(usuario.getRol().equals("Administrador")){
                    listener.exitoOperacionAdmin();
                }else{
                    listener.exitoOperacionVendedor();
                }

               // } else {
                    //listener.exitoOperacionVendedor();
               // }
            }
        } else {
            listener.passwordError();
        }
        adminUsuario.cerrar();
    }
}