package poo2018.fi.unju.edu.ar.trabajopracticofinal.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import poo2018.fi.unju.edu.ar.trabajopracticofinal.BDSqLite.ConsultaSQLite.SqLiteAdminUsuario;
import poo2018.fi.unju.edu.ar.trabajopracticofinal.R;
import poo2018.fi.unju.edu.ar.trabajopracticofinal.domain.Usuario;
import poo2018.fi.unju.edu.ar.trabajopracticofinal.util.AdaptadorTablaUsuario;

public class gestionUsuarioActivity extends AppCompatActivity {

    private ListView listView;
    private SqLiteAdminUsuario adminUsuario;
    private ArrayList<Usuario> usuarios = new ArrayList<>();
    private AdaptadorTablaUsuario adaptadorTablaUsuario;

    @Override
    protected void onPostResume() {
        super.onPostResume();
        crearTabla();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gestion_usuario);
        setupActionBar();
        listView = findViewById(R.id.list);
        adminUsuario = new SqLiteAdminUsuario(this);
        crearTabla();
    }

    public void crearTabla(){
        usuarios =  adminUsuario.getUsuarios();
        adaptadorTablaUsuario = new AdaptadorTablaUsuario(getApplicationContext(),usuarios);
        listView.setAdapter(adaptadorTablaUsuario);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(),usuarios.get(position).getDni(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void altaUsuario(View v){
        Intent activity = new Intent(this, AltaUsuarioActivity.class);
        startActivity(activity);
    }


    private void setupActionBar(){
        ActionBar actionBar=getSupportActionBar();
        if(actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            //actionBar.setTitle("Trabajo Practico");
        }
    }


}
