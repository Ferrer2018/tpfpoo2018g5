package poo2018.fi.unju.edu.ar.trabajopracticofinal.BDSqLite.util;

/**
 * Clase que crear las tablas de la BD.
 *
 * @author Maxi-Diaz
 */

public class TablaBD {

    /**
     * tabla usuarios
     */
    public static final String TABLA_USUARIO = "usuarios";
    public static final String CAMPO_ID = "id";
    public static final String CAMPO_NOMBRE  = "nombre";
    public static final String CAMPO_PASSWORD  = "password";
    public static final String CAMPO_DNI  = "dni";
    public static final String CAMPO_ROL  = "rol";

    /**
     * Sentencia para crear la tabla en la BD.
     */
    public static final String CREAR_TABLA_USUARIO = "CREATE TABLE "+TABLA_USUARIO
            +"("+CAMPO_ID+" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"+CAMPO_NOMBRE+" TEXT, " +
            ""+CAMPO_PASSWORD+" TEXT,"+CAMPO_DNI+" INTEGER,"+CAMPO_ROL+" TEXT)";
    public static final String defaultUser = " INSERT INTO " +TABLA_USUARIO+ " VALUES " + "( '01', 'admin', 'admin', 35421254, 'Administrador')";
    public static final String defaultUser1 = " INSERT INTO " +TABLA_USUARIO+ " VALUES " + "( '02', 'test', 'test', 35421253, 'Vendedor')";

    /**
     * tabla productos
     */
    public static final String TABLA_PRODUCTO = "productos";
    public static final String PRODUCTO_ID = "id";
    public static final String PRODUCTO_NOMBRE  = "nombre";
    public static final String PRODUCTO_TAMANIO = "tamanio";
    public static final String PRODUCTO_PRECIO= "precioU";
    public static final String PRODUCTO_ESTADO  = "estado";
    public static final String PRODUCTO_STOCK  = "stock";

    public static final String CREAR_TABLA_PRODUCTO = "CREATE TABLE "+TABLA_PRODUCTO
            +"("+PRODUCTO_ID+" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "+PRODUCTO_NOMBRE+" TEXT,"+PRODUCTO_TAMANIO+" INTEGER, "
            +PRODUCTO_PRECIO+" DOUBLE, "+PRODUCTO_ESTADO+" TEXT,"+PRODUCTO_STOCK+" INTEGER)";
    public static final String defaultProducto = " INSERT INTO " +TABLA_PRODUCTO+ " VALUES " + "( '02', 'Coca-Cola', 500, 50.0, 'Disponible', 50)";

    /**
     * tabla clientes
     */
    public static final String TABLA_CLIENTE = "clientes";
    public static final String CLIENTE_ID = "id";
    public static final String CLIENTE_NOMBRE = "nombre";
    public static final String CLIENTE_CUIT = "cuit";
    public static final String CLIENTE_DOMICILIO = "domicilio";

    public static final String CREAR_TABLA_CLIENTE = "CREATE TABLE " +TABLA_CLIENTE
            +"("+CLIENTE_ID+" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "+CLIENTE_NOMBRE+" TEXT, "+CLIENTE_CUIT+" TEXT, "
            +CLIENTE_DOMICILIO+" TEXT) ";

    /**
     * tabla pedido
     */
    public static final String TABLA_PEDIDO = "pedido";
    public static final String PEDIDO_ID = "id";
    //representa la clave foranea de producto
    public static final String PEDIDO_ID_VENDEDOR = "id_vendedor";
    public static final String PEDIDO_FECHA  = "fechaPedido";
    public static final String PEDIDO_ESTADO = "estadoPedido";
    //representa la clave foranea de producto
    public static final String PEDIDO_PRODUCTO_ID = "idProducto";
    //representa la clave foranea de cliente
    public static final String PEDIDO_CLIENTE_ID = "idCliente";
    public static final String PEDIDO_TOTAL = "idCliente";

    public static final String CREAR_TABLA_PEDIDO = "CREATE TABLE "+TABLA_PEDIDO
            +"("+PEDIDO_ID+" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "+PEDIDO_ID_VENDEDOR+" INTEGER, "+PEDIDO_FECHA+" TEXT, "+PEDIDO_ESTADO+" TEXT, " +
            ""+PEDIDO_PRODUCTO_ID+" INTEGER ," +PEDIDO_CLIENTE_ID+ "INTEGER, "+PEDIDO_TOTAL+ " DOUBLE)" ;

    /**
     * tabla detalle
     */
    public static final String TABLA_DETALLE = "detalle";
    public static final String DETALLE_ID_PEDIDO = "id_pedido";
    public static final String DETALLE_ITEM = "numeroItem";
    public static final String DETALLE_CANTIDAD = "cantidad";
    //representa la clave foranea de producto
    public static final String DETALLE_PRODUCTO_ID = "idProducto";
    public static final String DETALLE_SUBTOTAL = "subtotal";

    public static final String CREAR_TABLA_DETALLE = "CREATE TABLE "+TABLA_DETALLE
            + "("+DETALLE_ID_PEDIDO+ " INTEGER, "+DETALLE_ITEM+ " INTEGER, "+DETALLE_CANTIDAD
            + " TEXT, " + DETALLE_PRODUCTO_ID+ " INTEGER,"+DETALLE_SUBTOTAL+ " INTEGER) ";
}
