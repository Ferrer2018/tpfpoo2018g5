package poo2018.fi.unju.edu.ar.trabajopracticofinal.views;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import java.util.ArrayList;

import poo2018.fi.unju.edu.ar.trabajopracticofinal.BDSqLite.ConsultaSQLite.SqLiteAdminProducto;
import poo2018.fi.unju.edu.ar.trabajopracticofinal.R;
import poo2018.fi.unju.edu.ar.trabajopracticofinal.domain.Producto;
import poo2018.fi.unju.edu.ar.trabajopracticofinal.util.AdaptadorTablaProducto;

public class GestionProductosActivity extends AppCompatActivity {

    private ListView listView;
    private SqLiteAdminProducto adminProducto;
    private ArrayList<Producto> listaproductos = new ArrayList<>();
    private AdaptadorTablaProducto adaptadorTablaProducto;

    @Override
    protected void onPostResume() {
        super.onPostResume();
        crearTabla();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gestion_productos);
        setupActionBar();
        adminProducto = new SqLiteAdminProducto(this);
        listView = findViewById(R.id.list);
        crearTabla();
    }

    public void crearTabla(){
        listaproductos = adminProducto.getProductos();
        adaptadorTablaProducto = new AdaptadorTablaProducto(getApplicationContext(),listaproductos);
        listView.setAdapter(adaptadorTablaProducto);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Producto producto = listaproductos.get(position);
                Intent intent = new Intent(GestionProductosActivity.this,ModificarProductoActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("productos" ,producto);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    public void altaProducto(View v){
        Intent activity = new Intent(this,AltaProductoActivity.class);
        startActivity(activity);
    }

    private void setupActionBar(){
        ActionBar actionBar=getSupportActionBar();
        if(actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }
}
