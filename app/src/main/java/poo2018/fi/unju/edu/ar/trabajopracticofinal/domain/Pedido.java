package poo2018.fi.unju.edu.ar.trabajopracticofinal.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Pedido {
    private int idpedido;
    private String estadopedido;
    private String fechapedido;
    private Producto producto;
    private Usuario usuario;
    private Cliente cliente;
    private Double total;

    public Pedido(int idpedido, String estadopedido, String fechapedido, Producto producto,Usuario usuario, Cliente cliente,Double total) {
        this.idpedido = idpedido;
        this.estadopedido = estadopedido;
        this.fechapedido = fechapedido;
        this.producto = producto;
        this.usuario = usuario;
        this.cliente = cliente;
        this.total = total;
    }

    public Pedido() {
    }

    public int getIdpedido() {
        return idpedido;
    }

    public void setIdpedido(int idpedido) {
        this.idpedido = idpedido;
    }

    public String getEstadopedido() {
        return estadopedido;
    }

    public void setEstadopedido(String estadopedido) {
        this.estadopedido = estadopedido;
    }

    public String getFechapedido() {
        return fechapedido;
    }

    public void setFechapedido(String fechapedido) {
        this.fechapedido = fechapedido;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }


    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

}
