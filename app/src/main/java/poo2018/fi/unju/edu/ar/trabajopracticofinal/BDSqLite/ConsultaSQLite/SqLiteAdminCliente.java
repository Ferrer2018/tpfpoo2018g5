package poo2018.fi.unju.edu.ar.trabajopracticofinal.BDSqLite.ConsultaSQLite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import poo2018.fi.unju.edu.ar.trabajopracticofinal.BDSqLite.SQLiteHelper;
import poo2018.fi.unju.edu.ar.trabajopracticofinal.domain.Cliente;
import poo2018.fi.unju.edu.ar.trabajopracticofinal.BDSqLite.util.TablaBD;

public class SqLiteAdminCliente {

    private SQLiteHelper sqLiteHelper;
    private TablaBD tablaBD;
    private SQLiteDatabase db;

    /**
     * Constructor.
     *
     * @param context
     */

    public SqLiteAdminCliente(Context context) {
        //nos crea la base de datos
        sqLiteHelper = new SQLiteHelper(context);
    }

    /**
     * Abre la conexión con la base de datos.
     */

    public void abrir() {
        db = sqLiteHelper.getWritableDatabase();
    }

    /**
     * Consulta la conexión de la base de datos.
     */

    public void lectura() {
        db = sqLiteHelper.getReadableDatabase();
    }

    /**
     * Cierra la conexión de la base de datos.
     */

    public void cerrar() {
        db.close();
    }

    /**
     * Inserta un registro en la tabla Clientes.
     * @param cliente
     * @return
     */
    public long insertarCliente(Cliente cliente){
        ContentValues valores = new ContentValues();
        valores.put(tablaBD.CLIENTE_CUIT, cliente.getCuit());
        valores.put(tablaBD.CLIENTE_NOMBRE, cliente.getNombre());
        valores.put(tablaBD.CLIENTE_DOMICILIO, cliente.getDomicilio());

        return db.insert(tablaBD.TABLA_CLIENTE,null,valores);

    }

    /**
     * Modifica un registro de la tabla Clientes.
     * @param cliente
     * @return
     */
    public long modificarCliente(Cliente cliente){
        ContentValues valores = new ContentValues();
        valores.put(tablaBD.CLIENTE_ID, cliente.getIdcliente());
        valores.put(tablaBD.CLIENTE_CUIT, cliente.getCuit());
        valores.put(tablaBD.CLIENTE_NOMBRE, cliente.getNombre());
        valores.put(tablaBD.CLIENTE_DOMICILIO, cliente.getDomicilio());

        sqLiteHelper.getWritableDatabase().insert("cliente",null,valores);
        return db.update(tablaBD.TABLA_CLIENTE, valores, tablaBD.CLIENTE_ID +"=" + cliente.getIdcliente(), null);

    }

    /**
     * Elimina un registro de la tabla Clientes.
     * @param id
     * @return
     */
    public int eliminarCliente(int id){
        return db.delete(String.valueOf(tablaBD), tablaBD.CLIENTE_ID + "=" + id, null);
    }

    /**
     * Obtiene una lista de clientes.
     * @return
     */
    public ArrayList<Cliente> getClientes() {
        ArrayList<Cliente> lista = new ArrayList<>();
        lectura();
        String cadenaQuery = "SELECT * FROM " + tablaBD.TABLA_CLIENTE;
        Cursor cursor = db.rawQuery(cadenaQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                int id = cursor.getInt(cursor.getColumnIndex("id"));
                String cuit = cursor.getString(cursor.getColumnIndex("cuit"));
                String nombre = cursor.getString(cursor.getColumnIndex("nombre"));
                String domicilio = cursor.getString(cursor.getColumnIndex("domicilio"));
                Cliente cliente = new Cliente(id,cuit,nombre,domicilio);
                lista.add(cliente);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return lista;
    }
}
