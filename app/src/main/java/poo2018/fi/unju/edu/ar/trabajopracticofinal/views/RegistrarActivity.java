package poo2018.fi.unju.edu.ar.trabajopracticofinal.views;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import poo2018.fi.unju.edu.ar.trabajopracticofinal.BDSqLite.ConsultaSQLite.SqLiteAdminUsuario;
import poo2018.fi.unju.edu.ar.trabajopracticofinal.R;
import poo2018.fi.unju.edu.ar.trabajopracticofinal.domain.Usuario;

public class RegistrarActivity extends AppCompatActivity {

    Button btnNuevo;
    EditText txtNombre, txtDni, txtPass;
    Spinner spnRol;
    Usuario usuario;
    SqLiteAdminUsuario adminUsuario;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alta_usuario);
        context = this;
        btnNuevo = findViewById(R.id.btnNuevoUsuario);
        txtNombre = findViewById(R.id.txtNombre);
        txtDni = findViewById(R.id.txtDni);
        txtPass = findViewById(R.id.txtPass);
        spnRol = (Spinner) findViewById(R.id.spinnerRol);
        crearUsuario();
    }

    public void crearUsuario(){
        adminUsuario = new SqLiteAdminUsuario(this);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.rol, R.layout.support_simple_spinner_dropdown_item);
        spnRol.setAdapter(adapter);
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adminUsuario.abrir();
                usuario = new Usuario();
                usuario.setNombre(txtNombre.getText().toString());
                usuario.setDni(txtDni.getText().toString());
                usuario.setPassword(txtPass.getText().toString());
                usuario.setRol(spnRol.getSelectedItem().toString());
                long registro = adminUsuario.insertarUsuario(usuario);
                if (registro > 0) {
                    Toast.makeText(context, "Registro exitoso!", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(context, "No se pudo registrar!", Toast.LENGTH_LONG).show();
                }
                adminUsuario.cerrar();
                finish();
            }
        });
    }

    public void cancelar(View view){
        Intent activity = new Intent(this, Login.class);
        startActivity(activity);
    }
}
