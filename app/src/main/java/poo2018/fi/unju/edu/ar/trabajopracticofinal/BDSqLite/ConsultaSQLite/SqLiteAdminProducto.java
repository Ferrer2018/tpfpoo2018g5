package poo2018.fi.unju.edu.ar.trabajopracticofinal.BDSqLite.ConsultaSQLite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import java.util.ArrayList;

import poo2018.fi.unju.edu.ar.trabajopracticofinal.BDSqLite.SQLiteHelper;
import poo2018.fi.unju.edu.ar.trabajopracticofinal.domain.Producto;
import poo2018.fi.unju.edu.ar.trabajopracticofinal.BDSqLite.util.TablaBD;

/**
 * Clase que gestiona la BD de la aplicacion.
 *
 * @author Maxi-Diaz
 */

public class SqLiteAdminProducto {

    private SQLiteHelper sqLiteHelper;
    private TablaBD tablaBD;
    private SQLiteDatabase db;

    /**
     * Constructor.
     * @param context
     */
    public SqLiteAdminProducto(Context context){
        //nos crea la base de datos
        sqLiteHelper = new SQLiteHelper(context);
    }

    /**
     * Abre la conexión con la base de datos.
     */

    public void abrir() {
        db = sqLiteHelper.getWritableDatabase();
    }

    /**
     * Consulta la conexión de la base de datos.
     */

    public void lectura() {
        db = sqLiteHelper.getReadableDatabase();
    }

    /**
     * Cierra la conexión de la base de datos.
     */

    public void cerrar() {
        db.close();
    }

    /**
     * Inserta un registro en la tabla Productos.
     * @param producto
     * @return
     */
    public long insertarProducto(Producto producto){
        ContentValues valores = new ContentValues();
        valores.put(tablaBD.PRODUCTO_NOMBRE, producto.getNombre());
        valores.put(tablaBD.PRODUCTO_TAMANIO, producto.getTamaño());
        valores.put(tablaBD.PRODUCTO_PRECIO, producto.getPrecioU());
        valores.put(tablaBD.PRODUCTO_ESTADO, producto.getEstado());
        valores.put(tablaBD.PRODUCTO_STOCK, producto.getStock());
        return db.insert(tablaBD.TABLA_PRODUCTO,null,valores);
    }

    /**
     * Modifica un registro de la tabla Productos.
     * @param producto
     * @return
     */
    public long modificarProducto(Producto producto){
        ContentValues contentValues = new ContentValues();
        contentValues.put(tablaBD.PRODUCTO_NOMBRE,producto.getNombre());
        contentValues.put(tablaBD.PRODUCTO_TAMANIO,producto.getTamaño());
        contentValues.put(tablaBD.PRODUCTO_PRECIO,producto.getPrecioU());
        contentValues.put(tablaBD.PRODUCTO_ESTADO,producto.getEstado());
        contentValues.put(tablaBD.PRODUCTO_STOCK,producto.getStock());
        return db.update(tablaBD.TABLA_PRODUCTO, contentValues, tablaBD.PRODUCTO_ID +"=" + producto.getIdproducto(), null);
    }

    /**
     * Obtiene un lista de productos
     * @return
     */
    public ArrayList<Producto> getProductos() {
        ArrayList<Producto> lista = new ArrayList<>();
        lectura();
        String cadenaQuery = "SELECT * FROM " + tablaBD.TABLA_PRODUCTO;
        Cursor cursor = db.rawQuery(cadenaQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                int id = cursor.getInt(cursor.getColumnIndex("id"));
                String nombre = cursor.getString(cursor.getColumnIndex("nombre"));
                int tamaño = cursor.getInt(cursor.getColumnIndex("tamanio"));
                double precioU = cursor.getDouble(cursor.getColumnIndex("precioU"));
                String estado = cursor.getString(cursor.getColumnIndex("estado"));
                int stock = cursor.getInt(cursor.getColumnIndex("stock"));
                Producto producto = new Producto(id,nombre,tamaño,precioU,estado,stock);
                lista.add(producto);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return lista;
    }
}
