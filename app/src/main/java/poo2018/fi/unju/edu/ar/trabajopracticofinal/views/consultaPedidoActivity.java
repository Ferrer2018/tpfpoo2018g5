package poo2018.fi.unju.edu.ar.trabajopracticofinal.views;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

import poo2018.fi.unju.edu.ar.trabajopracticofinal.R;

public class consultaPedidoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consulta_pedido);
        setupActionBar();
    }

    private void setupActionBar(){
        ActionBar actionBar=getSupportActionBar();
        if(actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            //actionBar.setTitle("ejemplo");
        }
    }
}
