package poo2018.fi.unju.edu.ar.trabajopracticofinal.util;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import poo2018.fi.unju.edu.ar.trabajopracticofinal.R;
import poo2018.fi.unju.edu.ar.trabajopracticofinal.domain.Detalle;

public class AdaptadorTablaDetallePedido extends BaseAdapter {

    ArrayList<Detalle> itemList;
    Context context;

    public AdaptadorTablaDetallePedido(ArrayList<Detalle> itemList, Context context) {
        this.itemList = itemList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public Object getItem(int position) {
        return itemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.item_tabla_producto, parent, false);
        TextView lblProductoD =(TextView) itemView.findViewById(R.id.lblProductoD);
        TextView lblTamañoD =(TextView) itemView.findViewById(R.id.lblTamañoD);
        TextView lblUnidadesD =(TextView) itemView.findViewById(R.id.lblUnidadesD);
        TextView lblValorD =(TextView) itemView.findViewById(R.id.lblValorD);
        TextView lblSubTotalD =(TextView) itemView.findViewById(R.id.lblSubTotalD);
        lblProductoD.setText(""+itemList.get(position).getProducto());
        if (position%2==0){
            itemView.setBackgroundColor(Color.argb(255,248,248,248));
        } else {
            itemView.setBackgroundColor(Color.argb(255,255,255,255));
        }
        return itemView;
    }
}
