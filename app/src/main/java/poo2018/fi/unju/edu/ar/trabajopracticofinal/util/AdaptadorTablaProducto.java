package poo2018.fi.unju.edu.ar.trabajopracticofinal.util;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import poo2018.fi.unju.edu.ar.trabajopracticofinal.R;
import poo2018.fi.unju.edu.ar.trabajopracticofinal.domain.Producto;

public class AdaptadorTablaProducto extends BaseAdapter {

    ArrayList<Producto> itemList;
    Context context;

    public AdaptadorTablaProducto(Context context, ArrayList<Producto> itemList) {
        this.itemList = itemList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public Object getItem(int position) {
        return itemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.item_tabla_producto, parent, false);
        TextView lblNombreP =(TextView) itemView.findViewById(R.id.lblNombreP);
        TextView lblTamañoP =(TextView) itemView.findViewById(R.id.lblTamañoP);
        TextView lblPrecioP =(TextView) itemView.findViewById(R.id.lblPrecioP);
        TextView lblEstadoP =(TextView) itemView.findViewById(R.id.lblEstadoP);
        TextView lblStockP =(TextView) itemView.findViewById(R.id.lblStockP);
        lblNombreP.setText(""+itemList.get(position).getNombre());
        lblTamañoP.setText(""+itemList.get(position).getTamaño());
        lblPrecioP.setText(""+itemList.get(position).getPrecioU());
        lblEstadoP.setText(""+itemList.get(position).getEstado());
        lblStockP.setText(""+itemList.get(position).getStock());
        if (position%2==0){
            itemView.setBackgroundColor(Color.argb(255,248,248,248));
        } else {
            itemView.setBackgroundColor(Color.argb(255,255,255,255));
        }
        return itemView;
    }
}
