package poo2018.fi.unju.edu.ar.trabajopracticofinal.Presenters;

import android.content.Context;

import poo2018.fi.unju.edu.ar.trabajopracticofinal.Interactors.LoginInteractorImpl;
import poo2018.fi.unju.edu.ar.trabajopracticofinal.Interfaces.ILogin;

public class LoginPresenterImpl implements ILogin.Presenter, ILogin.Listener {

    private ILogin.view view;
    private ILogin.Interactor interactor;

    public LoginPresenterImpl(ILogin.view view, Context context) {
        this.view = view;
        interactor = new LoginInteractorImpl(this, context);
    }

    @Override
    public void validarUsuario(String pass) {
        interactor.validarUser(pass,this);
    }

    @Override
    public void passwordError() {
        if(view != null) {
            view.setErrorPassword();
        }
    }

    @Override
    public void showUserNoValido() {
        if(view != null) {
            view.showPasswordError();
        }
    }

    @Override
    public void exitoOperacionAdmin() {
        view.navigateToHomeAdmin();
        view.showPassword();
    }

    @Override
    public void exitoOperacionVendedor() {
        view.navigateToHomeVendedor();
        view.showPassword();
    }
}

