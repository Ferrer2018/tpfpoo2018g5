package poo2018.fi.unju.edu.ar.trabajopracticofinal.views;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

import poo2018.fi.unju.edu.ar.trabajopracticofinal.R;

public class NuevoPedidoActivity extends AppCompatActivity {

    private Spinner spinnerCliente;
    private Spinner spinnerProducto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuevo_pedido);

        spinnerCliente=(Spinner) findViewById(R.id.spinnerCliente);
        spinnerProducto=(Spinner) findViewById(R.id.spinnerProducto);
        //---------------------
        ArrayList<String> clientes =new ArrayList<String>();
        ArrayList<String> productos=new ArrayList<String>();

        clientes.add("Cliente 1");
        clientes.add("Cliente 2");
        clientes.add("Cliente 3");
        clientes.add("Cliente 4");

        productos.add("Sprite 350 cm3");
        productos.add("CocaCola 350 cm3");
        productos.add("Fanta 350 cm3");

        ArrayAdapter adp=new ArrayAdapter(NuevoPedidoActivity.this, android.R.layout.simple_spinner_dropdown_item, clientes);

        spinnerCliente.setAdapter(adp);

        spinnerCliente.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long l) {

                String cliente= (String) spinnerCliente.getAdapter().getItem(i);

                Toast.makeText(NuevoPedidoActivity.this, "seleccionaste: "+cliente, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter adaptador=new ArrayAdapter(NuevoPedidoActivity.this, android.R.layout.simple_spinner_dropdown_item, productos);

        spinnerProducto.setAdapter(adaptador);

        spinnerProducto.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        setupActionBar();
    }
    private void setupActionBar(){
        ActionBar actionBar=getSupportActionBar();
        if(actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }
}
