package poo2018.fi.unju.edu.ar.trabajopracticofinal.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import poo2018.fi.unju.edu.ar.trabajopracticofinal.BDSqLite.ConsultaSQLite.SqLiteAdminCliente;
import poo2018.fi.unju.edu.ar.trabajopracticofinal.R;
import poo2018.fi.unju.edu.ar.trabajopracticofinal.domain.Cliente;
import poo2018.fi.unju.edu.ar.trabajopracticofinal.util.AdaptadorTablaCliente;

public class gestionClienteActivity extends AppCompatActivity {

    private ListView listView;
    private SqLiteAdminCliente adminCliente;
    private ArrayList<Cliente> clientes = new ArrayList<>();
    private AdaptadorTablaCliente adaptadorTablaCliente;

    @Override
    protected void onPostResume() {
        super.onPostResume();
        crearTabla();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gestion_cliente);
        setupActionBar();
        listView = findViewById(R.id.list);
        adminCliente = new SqLiteAdminCliente(this);
        crearTabla();
    }

    public void crearTabla(){
        adaptadorTablaCliente = new AdaptadorTablaCliente(getApplicationContext(),adminCliente.getClientes());
        listView.setAdapter(adaptadorTablaCliente);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(),adminCliente.getClientes().get(position).getCuit(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void altaCliente(View v){
        Intent activity = new Intent(this, AltaClienteActivity.class);
        startActivity(activity);
    }

    private void setupActionBar(){
        ActionBar actionBar=getSupportActionBar();
        if(actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            //actionBar.setTitle("ejemplo");
        }
    }
}
