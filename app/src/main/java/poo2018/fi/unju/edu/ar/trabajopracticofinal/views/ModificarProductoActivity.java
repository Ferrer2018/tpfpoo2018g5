package poo2018.fi.unju.edu.ar.trabajopracticofinal.views;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import poo2018.fi.unju.edu.ar.trabajopracticofinal.BDSqLite.ConsultaSQLite.SqLiteAdminProducto;
import poo2018.fi.unju.edu.ar.trabajopracticofinal.R;
import poo2018.fi.unju.edu.ar.trabajopracticofinal.domain.Producto;

public class ModificarProductoActivity extends AppCompatActivity {

    Button btnNuevo;
    EditText txtNombremp, txtStockmp, txtPreciomp, txtTamañomp;
    RadioGroup radioGroup;
    RadioButton rdDisponiblemp, rdNoDisponiblemp;
    Producto producto;
    SqLiteAdminProducto adminProducto;
    Context context;
    String nombre, disponible, noDisponible;
    int stock, tamanio;
    double precioU;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modificar_producto);
        context = this;
        btnNuevo = findViewById(R.id.btnNuevo);
        txtNombremp = findViewById(R.id.txtNombremp);
        txtStockmp = findViewById(R.id.txtStockmp);
        txtPreciomp = findViewById(R.id.txtPreciomp);
        txtTamañomp = findViewById(R.id.txtTamañomp);
        rdDisponiblemp = findViewById(R.id.rdDisponiblemp);
        rdNoDisponiblemp = findViewById(R.id.rdNoDisponiblemp);
        Bundle objetoEnviado = getIntent().getExtras();
        Producto producto = null;
        if (objetoEnviado != null){
            producto = (Producto) objetoEnviado.getSerializable("producto");
            nombre = objetoEnviado.getString("nombre");
        }
        modificarProducto();
    }

    private void modificarProducto() {
        adminProducto = new SqLiteAdminProducto(this);
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adminProducto.abrir();
                producto = new Producto();
                producto.setNombre(txtNombremp.getText().toString());
                producto.setTamaño(Integer.parseInt(txtTamañomp.getText().toString()));
                producto.setPrecioU(Integer.parseInt(txtPreciomp.getText().toString()));
                if(rdDisponiblemp.isChecked()){
                    producto.setEstado(rdDisponiblemp.getText().toString());
                }else{
                    producto.setEstado(rdNoDisponiblemp.getText().toString());
                }
                producto.setStock(Integer.parseInt(txtStockmp.getText().toString()));
                long registro = adminProducto.modificarProducto(producto);
                if (registro > 0) {
                    Toast.makeText(context, "Producto fue registrado con exito!", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(context, "No se pudo registrar el Producto", Toast.LENGTH_LONG).show();
                }
                adminProducto.cerrar();
                finish();
            }
        });

    }

    public void cancelarProducto(View v){
        Intent activity = new Intent(this, GestionProductosActivity.class);
        startActivity(activity);
    }
}
